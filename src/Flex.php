<?php

namespace Kloud\Flex;

use Kloud\Flex\Commands\Compile;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\InputInterface;

class Flex extends Application
{
    public function __construct()
    {
        parent::__construct('Kloud Lex Compiler', '1.0.0');
    }

    protected function getCommandName(InputInterface $input)
    {
        return 'compile';
    }

    protected function getDefaultCommands()
    {
        return array_merge(
            parent::getDefaultCommands(),
            [new Compile()]
        );
    }

    public function getDefinition()
    {
        $inputDefinition = parent::getDefinition();
        $inputDefinition->setArguments();

        return $inputDefinition;
    }
}
