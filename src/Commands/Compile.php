<?php

namespace Kloud\Flex\Commands;

use Kloud\Flex\File;
use Kloud\Flex\Services\GccCompiler;
use Kloud\Flex\Services\LexCompiler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Compile extends Command
{
    protected function configure()
    {
        $this->configureCommand()
             ->configureArguments()
             ->configureOptions();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $overwrite = $input->getOption('overwrite');
        $lexFile   = $input->getArgument('lex-file');
        $path      = $input->getOption('path') ?: getcwd();
        $run       = $input->getOption('run');
        $content   = $input->getArgument('content-file');

        $file = File::from($path, $lexFile)
                    ->compile(new LexCompiler($overwrite))
                    ->compile(new GccCompiler($overwrite))
                    ->run($run, $content);
    }

    protected function configureCommand()
    {
        $this->setName('compile')
             ->setDescription('Compile lex')
             ->setHelp('Compile a "*.lex" file to an executable file');

        return $this;
    }

    protected function configureArguments()
    {
        $this->addArgument(
            'lex-file',
            InputArgument::REQUIRED,
            'The name of the ".lex" file to be compiled'
        );

        $this->addArgument(
            'content-file',
            InputArgument::OPTIONAL,
            'The full name of a content file to be passed to the executable',
            null
        );

        return $this;
    }

    protected function configureOptions()
    {
        $this->addOption(
            'overwrite', 'o',
            InputOption::VALUE_NONE,
            'Force the overwriting of an existing compiled and executable files'
        );

        $this->addOption(
            'as', null,
            InputOption::VALUE_REQUIRED,
            'The outputted compiled and executable file names'
        );

        $this->addOption(
            'run', 'r',
            InputOption::VALUE_NONE,
            'Run the executable file after compilation'
        );

        $this->addOption(
            'path', 'p',
            InputOption::VALUE_REQUIRED,
            'The full path to the lex asset'
        );

        return $this;
    }
}
