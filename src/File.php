<?php

namespace Kloud\Flex;

use Kloud\Flex\Services\Compiler;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class File
{
    protected $path;
    protected $filename;
    protected $extension;
    protected $filesystem;

    public function __construct($path, $filename, $extension = 'lex')
    {
        $this->path       = $path;
        $this->filename   = $filename;
        $this->extension  = $extension;
        $this->filesystem = new Filesystem();
    }

    public static function from($path, $filename)
    {
        $file = new static($path, $filename);

        if (! $file->exists()) {
            throw new FileNotFoundException(null, 0, null, $file->getFullPath());
        }

        return $file;
    }

    public function compile(Compiler $compiler)
    {
        $compiler->compile($this);

        return $this;
    }

    public function run($run = false, $inputFile = null)
    {
        if ($run) {
            exec(escapeshellarg($this->getFullPath()));
        }

        return $this;
    }

    public function getFullPath($withoutExtension = false)
    {
        return $this->path
               . DIRECTORY_SEPARATOR
               . $this->filename
               . ($withoutExtension ? '' : ".{$this->extension}");
    }

    public function extension($name)
    {
        $this->extension = $name;

        return $this;
    }

    public function exists($file = null)
    {
        $file = is_null($file) ? $this->getFullPath() : $file;

        return $this->filesystem->exists($file);
    }
}
