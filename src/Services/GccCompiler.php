<?php

namespace Kloud\Flex\Services;

use Kloud\Flex\File;

class GccCompiler extends Compiler
{
    protected $outputExtension = 'out';

    public function compile(File $file)
    {
        $output = escapeshellarg($this->getOutputFile($file));
        $input  = escapeshellarg($file->getFullPath());

        exec("gcc {$input} -o {$output} -ll");

        $file->extension($this->outputExtension);
    }
}
