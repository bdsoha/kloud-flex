<?php

namespace Kloud\Flex\Services;

use Kloud\Flex\File;

abstract class Compiler
{
    protected $outputExtension = '';
    protected $overwrites;

    public function __construct($overwrites = false)
    {
        $this->overwrites = $overwrites;
    }

    abstract public function compile(File $file);

    protected function getOutputFile(File $file)
    {
        return $file->getFullPath(true) . '.' . $this->outputExtension;
    }

    protected function outputFileExists(File $file)
    {
        return $file->exists($this->getOutputFile($file));
    }
}
