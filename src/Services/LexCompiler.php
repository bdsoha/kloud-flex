<?php

namespace Kloud\Flex\Services;

use Kloud\Flex\File;

class LexCompiler extends Compiler
{
    protected $outputExtension = 'yy.c';

    public function compile(File $file)
    {
        $output = escapeshellarg($this->getOutputFile($file));
        $input  = escapeshellarg($file->getFullPath());
        exec("flex -o {$output} {$input}");

        $file->extension($this->outputExtension);
    }
}
